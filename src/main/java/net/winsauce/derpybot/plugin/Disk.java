package net.winsauce.derpybot.plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.DerpyPlayerProfile.Permission;
import net.winsauce.derpybot.DerpyPlayerProfile.Property;
import net.winsauce.derpybot.TeleportLocation;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


public class Disk {

	private HashMap<String, DerpyPlayerProfile> profileCache;
	
	private final String path;
	private FileConfiguration derpyConfig;
	
	public Disk(String _path) {
		path = _path;
		DerpyPlayerProfile.setDiskInstance(this);
		load();
		
		profileCache = new HashMap<String, DerpyPlayerProfile>();
		
	}
	

	
	private void load() {
		File derpyFile = new File(path);
		
		if(derpyFile.exists()) {
			derpyConfig = YamlConfiguration.loadConfiguration(derpyFile);
			
		} else {
			DerpyBotPlugin.log("Derpy configuration file does not exist");
			DerpyBotPlugin.log("Creating file...");
			derpyConfig = new YamlConfiguration();
			derpyConfig.set("profiles", new ArrayList<HashMap<String, Object>>());
			try {
				derpyConfig.save(derpyFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized boolean profileExists(String playerName) {
		DerpyPlayerProfile profile = profileCache.get(playerName);
		if(profile == null) {
			//Profile is not cached
			//Search for profile on disk
			
			//If derpyConfig is new and has no profiles; create it's entry
			if(derpyConfig.getList("profiles") == null) derpyConfig.set("profiles", new ArrayList<HashMap<String, Object>>());
			
			for(HashMap<String, Object> profileEntry :  (List<HashMap<String, Object>>)derpyConfig.getList("profiles")) {
				//If we have found the profile we are looking for
				if(profileEntry.get("name").toString().equalsIgnoreCase(playerName)) {
					profile = DerpyPlayerProfile.readProfile(profileEntry);
					//Cache this profile
					profileCache.put(playerName, profile);
					
					//return the newly cached profile
					return true;
				}
			}
			
			return false;
		} 
		
		return true;
	}
	
	public DerpyPlayerProfile getExistingProfile(String playerName) {
		return profileExists(playerName) ? getProfile(playerName) : null;
	}
	
	/**
	 * Get DerpyPlayerProfile instance by name. <br>
	 * If the profile does not exist, it will be created/cached/written to disk. <br>
	 * If you are testing for PlayerProfile in some kind of conditional, it is highly 
	 * recommended you use getExistingProfile() instead.
	 * @param playerName - name of the player 
	 * @return Corresponding DerpyPlayerProfile instance to the given playerName
	 */
	public synchronized DerpyPlayerProfile getProfile(String playerName) {
		if(profileExists(playerName)) {
			return profileCache.get(playerName);
		} else {
			//Create profile, cache, write to disk
			
			//Create profile entry on disk
			createProfile(playerName);
			//Create new profile into cache
			DerpyPlayerProfile newProfile = DerpyPlayerProfile.newProfile(playerName);
			profileCache.put(playerName, newProfile);
			
			return newProfile;
		}
	}
	
	public void writeProfile(DerpyPlayerProfile playerProfile) {
		HashMap<String, Object> profile = null;
		
		//Find profile to write
		//Fun Fact: in this compiler-time-unwinding, the Collection statement is only called once
		for(HashMap<String, Object> profileEntry : ((List<HashMap<String, Object>>)derpyConfig.get("profiles"))) {
			if(profileEntry.get("name").toString().equalsIgnoreCase(playerProfile.getName())) {
				profile = profileEntry;
				break;
			}
		}
		
		//Remove lists from profile (only remove if they exist, obviously!)
		for(String key : new String[] {"teleports", "properties", "permissions"}) if(profile.containsKey(key)) profile.remove(key);
		
		
		//Re-populate teleports, properties, permissions (only if they are non-empty!)
		//Teleports
		if(!playerProfile.getTeleports().isEmpty()) {
			//Teleports aren't primitive objects, they need to be converted to a String representing them
			List<String> teleports = new ArrayList<String>();
			for(TeleportLocation t : playerProfile.getTeleports()) teleports.add(t.getDiskString());
			profile.put("teleports", teleports);
		}
		
		//Properties
		if(!playerProfile.getProperties().isEmpty()) {
			//Teleports aren't primitive objects, they need to be converted to a String representing them
			List<String> propertyStrings = new ArrayList<String>();
			for(Property p : playerProfile.getProperties()) propertyStrings.add(p.toString());
			profile.put("properties", propertyStrings);
		}
		
		//Permissions
		if(!playerProfile.getPermissions().isEmpty()) {
			//Teleports aren't primitive objects, they need to be converted to a String representing them
			List<String> permissionStrings = new ArrayList<String>();
			for(Permission p : playerProfile.getPermissions()) permissionStrings.add(p.toString());
			profile.put("permissions", permissionStrings);
		}
		
		//Write to disk
		try {
			derpyConfig.save(new File(path));
		} catch (IOException e) {
			DerpyBotPlugin.log("Failed to write profiles to disk! Error while writing profile \"" + playerProfile.getName() + "\"");
		}
	}
	
	private void createProfile(String name) {
		//Profile on Disk is a Hashmap of tags/paths e.g. "name", "teleports" etc
		HashMap<String, Object> profile = new HashMap<String, Object>();
		profile.put("name", name);
		
		//Add Profile HashMap to config file
		((List<HashMap<String, Object>>)derpyConfig.get("profiles")).add(profile);
		
		//Write to disk
		try {
			derpyConfig.save(new File(path));
		} catch (IOException e) {
			DerpyBotPlugin.log("Failed to write profiles to disk!");
		}
	}
	
	public List<String> getAllUsernames() {
		List<HashMap<String, Object>> profiles = (List<HashMap<String, Object>>) derpyConfig.getList("profiles");
		
		ArrayList<String> profileNames = new ArrayList<String>();
		
		for(HashMap<String, Object> profile : profiles) {
			profileNames.add(profile.get("name").toString());
		}
		
		return profileNames;
	}
}
