package net.winsauce.derpybot.plugin;


import java.util.Collection;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.DerpyPlayerProfile.NewLoginProfile;
import net.winsauce.derpybot.commands.CommandTabCompleter;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.winsauce.derpybot.DerpyBot;

public class EventListener implements Listener {
	
	private Disk disk;
	
	public EventListener(Disk _disk) {
		disk = _disk;
	}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent playerCommandPreprocessEvent) {
		Player player = playerCommandPreprocessEvent.getPlayer();
		player.sendMessage(ChatColor.DARK_GRAY + "<" + player.getDisplayName() + "> " + playerCommandPreprocessEvent.getMessage());
		
		DerpyBotPlugin.log("<" + player.getDisplayName() + "> " + playerCommandPreprocessEvent.getMessage());
	}
	
	@EventHandler
	public void onPlayerAutoComplete(PlayerChatTabCompleteEvent playerAutoCompleteEvent) {
		List<String> tabs = CommandTabCompleter.tabComplete(playerAutoCompleteEvent.getChatMessage(), playerAutoCompleteEvent.getPlayer());
		Collection<String> keys = playerAutoCompleteEvent.getTabCompletions();
		for(String value : tabs) if(!keys.contains(value)) keys.add(value);
	}
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent playerChatEvent) {
		DerpyPlayerProfile profile = disk.getExistingProfile(playerChatEvent.getPlayer().getDisplayName());
		String message = playerChatEvent.getMessage();
		Player player = playerChatEvent.getPlayer();
		
		//!derpy
		if(message.startsWith("!") || profile.isTalkingToDerpy()) {
			if(message.equalsIgnoreCase("derpy") || !DerpyBotPlugin.instance.execute(message.charAt(0)=='!' ? message : "!" + message, player)) {
				if(message.startsWith("!")) {
					player.sendMessage(ChatColor.DARK_GRAY + "<" + player.getDisplayName() + "> " + message);
					DerpyBot.whisper("Command \"" + message.substring(1).split(" +")[0] + "\" is not recognised", player);
					playerChatEvent.setCancelled(true);
					return;
				}
				//Chat to Derpy
				else profile.chatToDerpy(message.substring(1));
				
			} else {
				//the command was recognised
				playerChatEvent.setCancelled(true);
				return;
			}
		} 
		
		
		ChatColor playerColour = disk.getExistingProfile(playerChatEvent.getPlayer().getDisplayName()).getChatColour();
		playerChatEvent.setFormat(playerChatEvent.getFormat().replace("<", playerColour + "<").replace(">", ">" + ChatColor.WHITE));
	}
	
	@EventHandler
	public void onPlayerLogout(PlayerQuitEvent playerQuitEvent) {
		DerpyPlayerProfile profile = disk.getExistingProfile(playerQuitEvent.getPlayer().getDisplayName());
		DerpyBotPlugin.chatColourHandler.playerLogout(profile);
		profile.logOut();
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent playerLoginEvent) {
		DerpyPlayerProfile loginProfile = disk.getProfile(playerLoginEvent.getPlayer().getDisplayName());
		DerpyBotPlugin.chatColourHandler.playerLogin(loginProfile);
		
		Player player = playerLoginEvent.getPlayer();
		//If first Login
		if(loginProfile instanceof NewLoginProfile && ((NewLoginProfile)loginProfile).firstLogin) {
			((NewLoginProfile) loginProfile).firstLogin = false;
			executeWelcomeMessageThread("Welcome, " + DerpyBotPlugin.chatColourHandler.getColouredUsername(player.getDisplayName()) + "!", player);
		} else {
			executeWelcomeMessageThread("Welcome back, " + DerpyBotPlugin.chatColourHandler.getColouredUsername(player.getDisplayName()) + ".", player);
		}
	}
	
	private void executeWelcomeMessageThread(final String msg, final Player player) {
		Thread t = new Thread() {
			
			@Override
			public void run() {
				try{
					Thread.sleep(230);
					DerpyBot.announce(msg, player.getDisplayName());
					DerpyBot.whisper("Use !derpy to open a chat with me!", player);
					DerpyBot.whisper("See /help DerpyBot for a list of commands", player);
				} catch (InterruptedException e) {}
			}
		};
		
		t.start();
	}
}
