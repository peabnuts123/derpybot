package net.winsauce.derpybot.plugin;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Logger;

import net.winsauce.derpybot.DerpyBot;
import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.DerpyPlayerProfile.Permission;
import net.winsauce.derpybot.commands.ClearTeleportCommand;
import net.winsauce.derpybot.commands.CreativeCommand;
import net.winsauce.derpybot.commands.DerpyCommand;
import net.winsauce.derpybot.commands.PermissionCommand;
import net.winsauce.derpybot.commands.PrivateCommand;
import net.winsauce.derpybot.commands.ReturnTeleportCommand;
import net.winsauce.derpybot.commands.ShareTeleportCommand;
import net.winsauce.derpybot.commands.StoreTpCommand;
import net.winsauce.derpybot.commands.TeleportInfoCommand;
import net.winsauce.derpybot.commands.ToggleDerpyCommand;
import net.winsauce.derpybot.commands.TpCommand;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class DerpyBotPlugin extends JavaPlugin {

	public static DerpyBotPlugin instance;
	
	private static Logger logger;
	
	public static Disk disk;
	public static DerpyBot derpyBot;
	public static ChatColourHandler chatColourHandler;
	
	private EventListener eventListener;
	
	/* TODO testing:
	 * Cancel onPlayerDeath event !?
	 */
	
	/* TODO Development
	 * 
	 * Handle /Reload 
	 * 
	 * Pandora Bot?!
	 * Request Colour
	 * Statistics
	 * 	- X mined for ALL BLOCKS
	 * set spawn point
	 * Announce on TP share
	 * design disposition system. Allowing regular users to use more commands on derpy
	 * Consider "Jail" - Revokes all rights (including block placement/dig) and blocks
	 * Patch update notification feature / MOTD
	 * Think about program concurrency. 
	 * Message of the day / login message
	 */
	
	//NON-DERPY
//	 * Programs extend others?
//	 * 		-I.e, Pirate Program might extend Standard, to allow all the standard commands, but with extra processing.
//	 * Gamemodes!
//	 * 		-UHC - PacketSetSlot
//	 * 		-Race For Wool center-based
//	 * 		-Zombie Survival Mode
//	 * 		-Derpy "Quests". Narrative, etc. 
	
	@Override
	public void onEnable() {
		instance = this;
		logger = getLogger();
		
		disk = new Disk(System.getProperty("user.dir") + File.separatorChar + getServer().getWorlds().get(0).getName() + File.separatorChar + "derpy.yml");
		derpyBot = new DerpyBot(this);
		eventListener = new EventListener(disk);
		
		getServer().getPluginManager().registerEvents(eventListener, this);
		
		chatColourHandler = new ChatColourHandler();
	}
	
	public synchronized static void log(Object o) {
		logger.info(ChatColor.stripColor(o.toString()));
	}
	
	public boolean execute(String line, Player player) {
		String[] tokens = line.substring(1).split(" +");
		
		String commandName = tokens[0];
//		if(disk.getExistingProfile(player.getName()).isTalkingToDerpy() && commandName.equalsIgnoreCase("derpy")) return false;
		
		PluginCommand command  = getServer().getPluginCommand(commandName);
		
		if(command != null) {
			//If player is Op, or command does not require Op
			if(player.isOp() || !(command.getPermission() != null)) {
				String[] args = tokens.length > 1 ? Arrays.copyOfRange(tokens, 1, tokens.length) : new String[0];

				getServer().getPluginManager().callEvent(new PlayerCommandPreprocessEvent(player, line));
				onCommand(player, command, commandName, args);
				
				return true;
			} else {
				DerpyBot.whisper("You must be Op to execute this command", player);
				return true;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Convenience Method. Takes the work out of parsing commands
	 * @param commandSender - CommandSender instance
	 * @param args - args for current command
	 * @param command - Current Command instance
	 * @param commandName - desired command as a String
	 * @param numArgs - minimum number of arguments this command needs
	 * @param permissions - List of permissions this command requires
	 * @return <b>True</b> if this command satisfies all requirements.
	 * 	<br> <b>False</b> if this is not the correct command <br>
	 */
	private boolean evaluateCommand(CommandSender commandSender, String[] args, Command command, String commandName, int numArgs, Permission... permissions) {
		if(command.getName().equalsIgnoreCase(commandName) || command.getAliases().contains(commandName.toLowerCase())) {
			if(args.length >= numArgs) {
				
				DerpyPlayerProfile profile = disk.getExistingProfile(commandSender.getName());
				
				for(Permission p : permissions) {
					if(!profile.hasPermission(p)) {
						DerpyBot.whisper("You do not have the required permission to use this command.", commandSender);
						return false;
					}
				}
				
				return true;
				
			} else {
				DerpyBot.whisper("Not enough arguments for command \"" + commandName + "\".", commandSender);
				DerpyBot.whisper(command.getUsage().replace("<command>", command.getName()), commandSender);
				
				return false;
			}
		} else return false;
	}
	
	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
		DerpyCommand derpyCommand = null;
		
		//Don't forget to instantiate derpyCommand in your conditional statement
		if(evaluateCommand(commandSender, args, command, "permission", 3)) derpyCommand = new PermissionCommand();
		else if(evaluateCommand(commandSender, args, command, "derpy", 0)) derpyCommand = new ToggleDerpyCommand();
		else if(evaluateCommand(commandSender, args, command, "storetp", 0)) derpyCommand = new StoreTpCommand();
		else if(evaluateCommand(commandSender, args, command, "cleartp", 1)) derpyCommand = new ClearTeleportCommand();
		else if(evaluateCommand(commandSender, args, command, "sharetp", 1)) derpyCommand = new ShareTeleportCommand(true);
		else if(evaluateCommand(commandSender, args, command, "unsharetp", 1)) derpyCommand = new ShareTeleportCommand(false);
		else if(evaluateCommand(commandSender, args, command, "tp", 0)) derpyCommand = new TpCommand();
		else if(evaluateCommand(commandSender, args, command, "tpinfo", 0)) derpyCommand = new TeleportInfoCommand();
		//FIXME propery command has no use atm
//		else if(evaluateCommand(commandSender, args, command, "property", 0)) derpyCommand = new PropertyCommand();
		else if(evaluateCommand(commandSender, args, command, "private", 0)) derpyCommand = new PrivateCommand();
		else if(evaluateCommand(commandSender, args, command, "creative", 0, Permission.su)) derpyCommand = new CreativeCommand();
		else if(evaluateCommand(commandSender, args, command, "returntp", 0)) derpyCommand = new ReturnTeleportCommand();
		
		if(derpyCommand != null) {
			derpyCommand.execute(commandSender, disk, args);
			return true;
		} else return false;
	}
	
	@Override
	public void onDisable() {
		//TODO stuff on disable?
	}
}
