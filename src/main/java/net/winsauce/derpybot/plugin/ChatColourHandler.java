package net.winsauce.derpybot.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import net.winsauce.derpybot.DerpyBot;
import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.exceptions.PlayerNotOnlineException;

import org.bukkit.ChatColor;

public class ChatColourHandler {
	public static ChatColor[] validColours = {
		ChatColor.AQUA,
		ChatColor.BLUE,
		ChatColor.DARK_AQUA,
		ChatColor.DARK_BLUE,
		ChatColor.DARK_GRAY,
		ChatColor.DARK_GREEN,
		ChatColor.DARK_PURPLE,
		ChatColor.DARK_RED,
		ChatColor.GOLD,
		ChatColor.GREEN,
		ChatColor.RED,
		ChatColor.WHITE,
		ChatColor.YELLOW
	};
	
	private HashMap<String, ChatColor> colourTable;
	private ArrayList<ChatColor> colourPool;
	
	public ChatColourHandler() {
		colourPool = new ArrayList<ChatColor>();
		colourTable = new HashMap<String, ChatColor>();
		
		//Populate Colour Pool with valid colours
		for(ChatColor c : ChatColor.values()) {
			if(!isValidChatColour(c)) continue;
			colourPool.add(c);
		}
		
	}

	public String getColouredUsername(ChatColor previousColour, String playerName) {
		try {
			return getPlayerChatColour(playerName) + playerName + previousColour;
		} catch (PlayerNotOnlineException e) {
			return playerName;
		}
	}
	
	public String getColouredUsername(String playerName) {
		return getColouredUsername(ChatColor.WHITE, playerName);
	}
	
	public ChatColor getPlayerChatColour(String playerName) throws PlayerNotOnlineException {
		if(playerName.equalsIgnoreCase(DerpyBot.USERNAME)) return ChatColor.GRAY;
		
//		Server server = DerpyBotPlugin.instance.getServer(); 
//		Team playerTeam = server.getScoreboardManager().getMainScoreboard().getPlayerTeam(server.getOfflinePlayer(playerName)); 
//		if(playerTeam != null) {
//			return null;
//		}else {
//		}
		
		//TODO be sensitive of Team Colours
		if(colourTable.containsKey(playerName.toLowerCase())) return colourTable.get(playerName.toLowerCase());
		else throw new PlayerNotOnlineException();
		
	}
	
	public void playerLogout(DerpyPlayerProfile playerProfile) {
		//Add colour back into colour pool
		colourPool.add(colourTable.get(playerProfile.getName().toLowerCase()));
		//Remove entry from colourTable
		colourTable.remove(playerProfile.getName().toLowerCase());
	}
	
	public void playerLogin(DerpyPlayerProfile playerProfile) {
		//Get requested colour by profile (hash function)
		ChatColor playerColour = playerProfile.getRequestedChatColour();
		
		DerpyBot.announce("DEBUG: \"" + playerProfile.getName() +"\" requesting colour: " + playerColour.name());
		
		//If requested colour is unavailable, use a random entry from the Colour Pool
		if(!colourPool.contains(playerColour)) {
			//If there are no colours left, just assign a random colour.
			if(colourPool.isEmpty()) {
				colourTable.put(playerProfile.getName().toLowerCase(), randomColour());
				DerpyBot.announce("DEBUG: NO AVAILABLE COLORS IN POOL. Assigning totally random color: " + playerColour.name());
				return;
			}
			
			
			Random r = new Random(System.currentTimeMillis());
			playerColour = colourPool.get(r.nextInt(colourPool.size()));
			
			DerpyBot.announce("DEBUG: COLOR COLLISION. Assigning random color from pool: " + playerColour.name());
		}
		
		//Add player colour entry
		colourTable.put(playerProfile.getName().toLowerCase(), playerColour);
		//Remove colour from colour pool
		colourPool.remove(playerColour);
		DerpyBot.announce("DEBUG: Assigning colour");
	}
	
	/**
	 * @return Random, valid chat Colour
	 */
	public ChatColor randomColour() {
		Random r = new Random(System.currentTimeMillis());
		ChatColor randomColour;
		do {
			randomColour = ChatColor.values()[r.nextInt(ChatColor.values().length)];
		} while(!isValidChatColour(randomColour));
		
		return randomColour;
	}
	
	/**
	 * Test if a ChatColour is valid for chat
	 * @param c ChatColor being tested
	 * @return True if it is not GRAY, BLACK, or LIGHT_PURPLE
	 */
	public boolean isValidChatColour(ChatColor c) {
		//True if c is not Gray, and c is not Block, and c is not light purple
		
		for(ChatColor test : validColours) if(test == c) return true;
		
		return false;
	}
}
