package net.winsauce.derpybot;

import net.winsauce.derpybot.plugin.DerpyBotPlugin;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportLocation {
	public static final String SEMI_COLON_SEQUENCE = "&SMC";
	
	public Location playerLocation;
	public String name;
	public boolean shared;
	public int index;
	
	public TeleportLocation(int index, String name, Location playerLocation) {
		this(index, name, playerLocation, false);
	}
	
	public TeleportLocation(int _index, String _name, Location location, boolean _shared) {
		index = _index;
		name = _name;
		playerLocation = location;
		shared = _shared;
	}
	
	public void updateLocation(Location l) {
		playerLocation = l;
	}
	
	public void teleport(Player p) {
		if(p.getLocation().getWorld().getName().equals(playerLocation.getWorld().getName())) {
			DerpyBotPlugin.disk.getExistingProfile(p.getDisplayName()).previous = p.getLocation();
			p.teleport(playerLocation);
		} else {
			DerpyBot.whisper("You cannot teleport across worlds unless teleporting to another player", p);
		}
	}
	
	public String getDiskString() {
		return index+";"+name.replace(";", SEMI_COLON_SEQUENCE)+";"+playerLocation.getWorld().getName()+";"+playerLocation.getX()+";"+playerLocation.getY()+";"+playerLocation.getZ()+";"+playerLocation.getYaw()+";"+playerLocation.getPitch()+";"+shared;
	}
	
	
	
	
	
	public static TeleportLocation readTeleportLocation(String locationEntry) {
		String[] values = locationEntry.split(";");
		
		return new TeleportLocation(Integer.parseInt(values[0]),
				values[1].replace(SEMI_COLON_SEQUENCE, ";"),
				new Location(DerpyBotPlugin.instance.getServer().getWorld(values[2]),
						Double.parseDouble(values[3]),
						Double.parseDouble(values[4]),
						Double.parseDouble(values[5]),
						Float.parseFloat(values[6]),
						Float.parseFloat(values[7])),
				Boolean.parseBoolean(values[8]));
	}
}
