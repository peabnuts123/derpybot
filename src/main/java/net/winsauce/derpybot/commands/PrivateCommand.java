package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class PrivateCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		
		if(args.length == 0 ) {
			//!private
			//Toggle private value
			playerProfile.inPrivate = !playerProfile.inPrivate;
			DerpyBot.whisper("Toggled private mode " + (playerProfile.inPrivate ? (ChatColor.GREEN + "on") : (ChatColor.RED + "off")), cs);
		} else {
			//!private on/off
			//set private to on/off
			if(args[0].equalsIgnoreCase("on")) {
				//!private on
				if(playerProfile.inPrivate) DerpyBot.whisper("Private mode is already " + ChatColor.GREEN + "on", cs);
				else {
					playerProfile.inPrivate = true;
					DerpyBot.whisper("Toggled private mode " + ChatColor.GREEN + "on", cs);
				}
				
			} else if(args[0].equalsIgnoreCase("off")) {
				//!private off
				if(!playerProfile.inPrivate) DerpyBot.whisper("Private mode is already " + ChatColor.RED + "off", cs);
				else {
					playerProfile.inPrivate = false;
					DerpyBot.whisper("Toggled private mode " + ChatColor.RED + "off", cs);
				}
			} else {
				//invalid
				DerpyBot.whisper("Invalid parameter. !private [on/off]", cs);
			}
		}
	}
	
	public static List<String> tabComplete(String word, int index) {
		ArrayList<String> list = new ArrayList<String>();
		
		//!private [on/off]
		if(index != 0) return list;
		
		if("on".startsWith(word.toLowerCase())) list.add("on");
		if("off". startsWith(word.toLowerCase())) list.add("off");
		
		return list;
	}

}
