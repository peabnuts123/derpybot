package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyBot;
import net.winsauce.derpybot.DerpyPlayerProfile.Permission;
import net.winsauce.derpybot.plugin.DerpyBotPlugin;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;

public class PermissionCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		Permission permission;

		//TODO  unPermission'd peeps can see what permissions they have
		//		- you mean list permissions?
		//		RIGHT- it's because permission is an op'd command. Whatever that's fine for now
//		if(args.length == 0) {
//			DerpyBot.whisper("Permissions: ", cs);
//			List<Permission> permissions = disk.getExistingProfile(cs.getName()).getPermissions();
//			for(Permission p : permissions) {
//				DerpyBot.whisper(p.toString(), cs);
//			}
//			
//			if(permissions.size() == 0) DerpyBot.whisper("You have no assigned permissions", cs);
//		} else 
		if(args[0].equalsIgnoreCase("give")) {
			//Give permission

			//If permission is not a real permission
			if(!Permission.isPermission(args[2])) {
				DerpyBot.whisper("The permission \"" + args[2] + "\" does not exist.", cs);
				return;
			} else permission = Permission.valueOf(args[2]);

			try {
				if(disk.getExistingProfile(args[1]).givePermission(permission)) {
					DerpyBot.whisper("Gave permission \"" + args[2] + "\" to " + args[1], cs);
					DerpyBot.whisper("You have been granted the permission \"" + args[2] + "\".", cs.getServer().getPlayer(args[1]));
				} else {
					DerpyBot.whisper("User \"" + args[1] + "\" already has permission \"" + args[2] + "\".", cs); 
				}
			} catch(NullPointerException e) {
				DerpyBot.whisper("Player \"" + args[1] + "\" has not been on this server", cs);
			}

		} else if(args[0].equalsIgnoreCase("take")) {
			//Take permission

			//If permission is not a real permission
			if(!Permission.isPermission(args[2])) {
				DerpyBot.whisper("The permission \"" + args[2] + "\" does not exist.", cs);
				return;
			} else permission = Permission.valueOf(args[2]);

			try {
				if(disk.getExistingProfile(args[1]).revokePermission(permission)) {
					DerpyBot.whisper("Revoked permission \"" + args[2] + "\" from " + args[1], cs);
					DerpyBot.whisper("You have lost the permission \"" + args[2] + "\".", cs.getServer().getPlayer(args[1]));
				} else {
					DerpyBot.whisper("User \"" + args[1] + "\" does not have permission \"" + args[2] + "\".", cs); 
				}
			} catch(NullPointerException e) {
				DerpyBot.whisper("Player \"" + args[1] + "\" has not been on this server", cs);
			}

		} else {
			//Neither give/take, incorrect usage
			DerpyBot.whisper("Incorrect Usage: ", cs);
			DerpyBot.whisper(cs.getServer().getPluginCommand("permission").getUsage().replace("<command>", "permission"), cs);
		}
	}

	public static List<String> tabCompleter(String word, int index) {
		ArrayList<String> list = new ArrayList<String>();
		
		//!permission <give/take> <player> <permission>
		switch(index) {
			case 0:
				if("give".startsWith(word.toLowerCase())) list.add("give");
				else if("take".startsWith(word.toLowerCase())) list.add("take");
				break;
			case 1:
				for(String username : DerpyBotPlugin.disk.getAllUsernames()) if(username.toLowerCase().startsWith(word.toLowerCase())) list.add(username);
			case 2:
				for(Permission p : Permission.values()) if(p.toString().toLowerCase().startsWith(word.toLowerCase())) list.add(p.toString());
				break;
		}
		
		return list;
	}
}
