package net.winsauce.derpybot.commands;

import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;

public abstract class DerpyCommand {

	public abstract void execute(CommandSender cs, Disk disk, String[] args);
	
}
