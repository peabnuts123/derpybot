package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.TeleportLocation;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class ClearTeleportCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		//!cleartp <index>
		//!cleartp <name>
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		
		try {
			int index = Integer.parseInt(args[0]);
			//!cleartp <index>
			
			if(playerProfile.removeTeleport(index)) DerpyBot.whisper("Cleared Teleport in slot " + args[0] + "", cs);
			else DerpyBot.whisper("There is not Teleport in slot " + args[0], cs);
		} catch(NumberFormatException e) {
			//!cleartp <name>
			if(playerProfile.removeTeleport(args[0])) DerpyBot.whisper("Cleared Teleport \"" + args[0] + "\"", cs);
			else DerpyBot.whisper("There is no teleport with name \"" + args[0] + "\"", cs);
		}
	}
	
	public static List<String> tabComplete(String word, int index, DerpyPlayerProfile profile) {
		ArrayList<String> list = new ArrayList<String>();
		
		if(index != 0) return list;
		
		for(TeleportLocation t : profile.getTeleports()) if(t.name.toLowerCase().startsWith(word.toLowerCase())) list.add(t.name);
		
		return list;
	}
}
