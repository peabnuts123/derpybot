package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.TeleportLocation;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class ShareTeleportCommand extends DerpyCommand {

	private boolean sharing;
	public ShareTeleportCommand(boolean _sharing) {
		sharing = _sharing;
	}
	
	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		
		//!sharetp <index>
		//!sharetp <name>
		
		TeleportLocation t;
		
		try {
			int index = Integer.parseInt(args[0]);
			t = playerProfile.getTeleport(index);
			
			if(t == null) {
				DerpyBot.whisper("There is no teleport at index " + args[0], cs);
				return;
			}
		} catch(NumberFormatException e) {
			t = playerProfile.getTeleport(args[0]);
			if(t == null) {
				DerpyBot.whisper("There is no teleport with name \"" + args[0] + "\"", cs);
				return;
			}
		}
		
		t.shared = sharing;
		playerProfile.onChanged();
		
		DerpyBot.whisper("Set teleport \"" + t.name + "\" " + (sharing ? (ChatColor.GREEN + "public") : (ChatColor.RED + "private")), cs);
	}

	public static List<String> tabComplete(String word, int index, DerpyPlayerProfile profile) {
		//!sharetp index/name
		ArrayList<String> list = new ArrayList<String>();
		
		if(index != 0) return list;
		
		for(TeleportLocation t : profile.getTeleports()) if(t.name.toLowerCase().startsWith(word.toLowerCase())) list.add(t.name);
		
		return list;
	}
}
