package net.winsauce.derpybot.commands;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.winsauce.derpybot.DerpyBot;

public class ReturnTeleportCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		Player p = ((Player)cs);
		
		if(playerProfile.previous != null) {
			if(p.getLocation().getWorld().equals(playerProfile.previous.getWorld())) {
				((Player)cs).teleport(playerProfile.previous);
			} else {
				DerpyBot.whisper("You cannot teleport across worlds unless teleporting to another player", p);
			}
		}
		else DerpyBot.whisper("You have not made any teleports since logging in", cs);
	}

}
