package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.DerpyPlayerProfile.Property;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class PropertyCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		//!property 
		//!property <name>
		//!property <name> <on/off>
		
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		List<Property> properties = playerProfile.getProperties();
		
		if(args.length == 0) {
			//!property
			//List properties / values
			DerpyBot.whisper("Properties: ", cs);
			for(Property p : DerpyPlayerProfile.Property.values()) 
				DerpyBot.whisper(p.toString() + " | " + (properties.contains(p) ? (ChatColor.GREEN + "on") : (ChatColor.RED + "off")), cs);
			
		} else if(args.length == 1) {
			//!property <name>
			//Display value for property
			Property p = DerpyPlayerProfile.Property.valueOf(args[0]);
			if(p != null) {
				DerpyBot.whisper(args[0] + " | " + (properties.contains(p) ? (ChatColor.GREEN + "on") : (ChatColor.RED + "off")), cs);
			} else {
				DerpyBot.whisper("No property with name \"" + args[0] + ". See !property for a list", cs);
			}
		} else {
			//!property <name> <value>
			//Set property value
			Property p = DerpyPlayerProfile.Property.valueOf(args[0]);
			if(p != null) {
				if(args[1].equalsIgnoreCase("on")) {
					//Successfully given
					if(playerProfile.giveProperty(p)) DerpyBot.whisper("Enabled property \"" + args[0] + "\"", cs);
					//Failed to give
					else DerpyBot.whisper("Property \"" + args[0] + "\" is already enabled", cs); 
				} else if(args[1].equalsIgnoreCase("off")) {
					//Successfully revoked
					if(playerProfile.revokeProperty(p)) DerpyBot.whisper("Disabled property \"" + args[0] + "\"", cs);
					//Failed to revoke
					else DerpyBot.whisper("Property \"" + args[0] + "\" is already disabled", cs);
				} else {
					DerpyBot.whisper("Invalid value for Property. Either on/off", cs);
				}
			} else {
				DerpyBot.whisper("No property with name \"" + args[0] + ". See !property for a list", cs);
			}
		}
		
	}

	public static List<String> tabComplete(String word, int index) {
		//!property <name> <on/off>
		ArrayList<String> list = new ArrayList<String>();

//		switch(index) {
//		case 0:
//			for(Property p : Property.values()) if(p.toString().toLowerCase().startsWith(word.toLowerCase())) list.add(p.toString());
//			break;
//		case 1:
//			if("on".startsWith(word.toLowerCase())) list.add("on");
//			if("off".startsWith(word.toLowerCase())) list.add("off");
//			break;
//		}

		return list;
	}
}
