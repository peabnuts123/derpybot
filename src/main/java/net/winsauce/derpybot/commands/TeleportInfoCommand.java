package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.TeleportLocation;
import net.winsauce.derpybot.plugin.DerpyBotPlugin;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class TeleportInfoCommand extends DerpyCommand {

	public static final int TELEPORTS_PER_PAGE = 10;
	
	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		
		DerpyPlayerProfile target;
		if(args.length == 1 || (cs.isOp() && args.length > 1)) {
			try {
				//!tpi <page index>
				
				//Hacks: if player is op, this is called on another player (displaying all TPs)
				target = (args.length > 1) ? disk.getExistingProfile(args[0]) : disk.getExistingProfile(cs.getName());
				String pageIndexString = (args.length > 1) ? args[1] : args[0];
				
				//Verify target has been online (this is always skipped if target is current player)
				if(target == null) {
					DerpyBot.whisper("Player \"" + args[0] + "\" has not been on this server", cs);
					return;
				}
				
				//Convert specified value to 0-based index
				int pageNumber = Integer.parseInt(pageIndexString) - 1;
				
				if(pageNumber < 0) {
					DerpyBot.whisper("Invalid page index value, must be at least 1", cs);
					return;
				}
				
				
				ArrayList<TeleportLocation> teleports = target.getTeleports();
				
				//pageStartingIndex is the first index to display - if first page, this will be 0
				int pageStartingIndex = pageNumber * TELEPORTS_PER_PAGE;
				
				//If there are enough teleports to make it to this page, i.e. if this page index is out of range
				//Does not break if this is the first page
				if(teleports.size() > pageStartingIndex || pageStartingIndex == 0) {
					//display page
					TeleportLocation t;
					DerpyBot.whisper("Stored teleports [" + pageIndexString + "/" + ((int)Math.ceil(teleports.size() / (float)TELEPORTS_PER_PAGE)) + "]:", cs);
					
					for(int i=0; i<TELEPORTS_PER_PAGE; i++) {
						try {
							//If player has no teleports this will break instantly (but title will still be displayed)
							t = teleports.get(i + pageStartingIndex);
							DerpyBot.whisper((t.shared ? ChatColor.GREEN + "public" : ChatColor.DARK_GRAY + "public") + ChatColor.GRAY + " | " + t.index + "  " + t.name, cs);
						} catch(IndexOutOfBoundsException e) { break; }
					}
				} else {
					DerpyBot.whisper("tpinfo page index out of range. Must be between [1/" + ((int)Math.ceil(teleports.size() / (float)TELEPORTS_PER_PAGE)) + "]", cs);
				}
				
			} catch(NumberFormatException e) {
				//!tpi <player>
				execute(cs, disk, new String[] {args[0], "1"});
			}
			
		} else if(args.length >= 2) {
			//!tpi <player> <page>
			
			//Get target player 'args[0]'
			target = disk.getExistingProfile(args[0]);
			try {
				ArrayList<TeleportLocation> teleports = target.getTeleports();
			
				//0-index the page number
				int pageNumber = Integer.parseInt(args[1]) - 1;
				
				if(pageNumber < 0) {
					DerpyBot.whisper("Invalid page index value, must be at least 1", cs);
					return;
				}
				
				//firstPageIndex is first value to display
				int firstEntryIndex = pageNumber * TELEPORTS_PER_PAGE;
				
				int sharedTotal = 0;
				int startIndex = -1;
				
				//Total how many shared TPs player has
				//Also find which tp is our first tp
				TeleportLocation t;
				for (int i = 0; i < teleports.size(); i++) {
					t = teleports.get(i);
					sharedTotal += t.shared ? 1 : 0;
					
					//if firstPageIndex == 0, the record to set sharedTotal to 1 is the first to dispaly
					//if firstPageIndex == 4 the record to setSharedTotal to 5 is the first to display, i.e 
					if(sharedTotal == firstEntryIndex+1) startIndex = i;
				}
				
				//sharedTotal : total number of shared TPs
				//startIndex : index of TP that starts the page
				
				
				//if startIndex == -1 it means there are not enough entries for this page, i.e. the page is out of bounds
				//If firstEntryIndex == 0 it's the first page, and must always work
				if(startIndex != -1 || firstEntryIndex == 0) {
					int sharedCount = 0;
					int index = startIndex;
					DerpyBot.whisper("Stored teleports for " + DerpyBotPlugin.chatColourHandler.getColouredUsername(ChatColor.GRAY, args[0]) 
						+ " [" + args[1] + "/" + ((int)Math.ceil(sharedTotal / (float)TELEPORTS_PER_PAGE)) + "]:", cs);
					while(sharedCount < TELEPORTS_PER_PAGE) {
						try {
							t = teleports.get(index);
							if(t.shared) {
								DerpyBot.whisper(ChatColor.GREEN + "public" + ChatColor.GRAY + " | " + t.index + "  " + t.name, cs);
								sharedCount++;
							}
							index++;
						} catch (IndexOutOfBoundsException e) { break; }
					}
				} else DerpyBot.whisper("tpinfo page index out of range. Must be between [1/" + ((int)Math.ceil(sharedTotal / (float)TELEPORTS_PER_PAGE)) + "]", cs);
			} catch (NumberFormatException e) {
				DerpyBot.whisper("Invalid value for tp index, must be a whole number", cs);
			} catch (NullPointerException e) {
				DerpyBot.whisper("Player \"" + args[0] + "\" has not been on this server", cs);
			}
		} else {
			//!tpi
			execute(cs, disk, new String[] {"1"});
		}
	}
	
	public static List<String> tabComplete(String word, int index) {
		//!tpi <player> <page>
		ArrayList<String> list = new ArrayList<String>();
		
		if(index != 0) return list;
		
		for(String username : DerpyBotPlugin.disk.getAllUsernames()) if(username.toLowerCase().startsWith(word.toLowerCase())) list.add(username);
		
		return list;
	}
}
