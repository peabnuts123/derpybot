package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.DerpyBot;
import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.TeleportLocation;
import net.winsauce.derpybot.plugin.DerpyBotPlugin;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		DerpyPlayerProfile profile = disk.getExistingProfile(cs.getName());
		TeleportLocation t;
		
		if(args.length == 0) {
			//tp
			
			//get Temporary stored location
			t = profile.temp;
			if(t == null) DerpyBot.whisper("There is no temporary location stored", cs);
			else t.teleport((Player)cs);
		} else if(args.length == 1) {
			//!tp <name/index/player>
			
			//Assume is player, if not, call function as <cs name> name/index
			profile = disk.getExistingProfile(args[0]);
			if(profile != null) {
				//!tp <player>
				Player p = cs.getServer().getPlayer(args[0]);
				if(p == null) DerpyBot.whisper("Player \"" + args[0] + "\" is not online", cs);
				else if(p.equals(cs)) DerpyBot.whisper("You cannot teleport to yourself!", cs);
				else {
					if(!profile.inPrivate) {
						disk.getProfile(cs.getName()).previous = ((Player)cs).getLocation();
						((Player)cs).teleport(p);
						DerpyBot.whisper("Teleporting \"" + cs.getName() + "\" to \"" + p.getDisplayName() + "\"", cs);
						DerpyBot.whisper("Teleporting \"" + cs.getName() + "\" to \"" + p.getDisplayName() + "\"", p);
					} else DerpyBot.whisper("Player \"" + p.getDisplayName() + "\" is in private mode.", cs);
				}
			} else {
				//This is not a player / this player has not been online
				execute(cs, disk, new String[] {cs.getName(), args[0]});
			}
		} else {
			//!tp <player> <name/index>
			profile = disk.getProfile(args[0]);
			
			try {
				//tp <player> <index>
				t = profile.getTeleport(Integer.parseInt(args[1]));
				
				if(t == null) DerpyBot.whisper("There is no TP stored in slot " + args[1], cs);
				else if(!(t.shared || cs.isOp() || args[0].equalsIgnoreCase(cs.getName()))) DerpyBot.whisper("Teleport \"" + t.name + "\" is not shared", cs);
				else t.teleport((Player)cs); 
					
			} catch (NumberFormatException e) {
				//tp <player> <name>
				t = profile.getTeleport(args[1]);
				
				if(t == null) DerpyBot.whisper("There is no teleport with name \"" + args[1] + "\"", cs);
				else if(!(t.shared || cs.isOp() || args[0].equalsIgnoreCase(cs.getName()))) DerpyBot.whisper("Teleport \"" + t.name + "\" is not shared", cs);
				else t.teleport((Player)cs);
			}
			
		}
	}

	public static List<String> tabComplete(String word, int index, String[] tokens, Player player) {
		ArrayList<String> list = new ArrayList<String>();
		
		//!tp <name>
		//!tp <player> <name>
		
		DerpyPlayerProfile target = null;
		boolean mustBeShared = false;
		
		switch(index) {
			case 0:
				//Personal TP (let's hope they don't have any TPs named like players)
				//Can be a username
				target = DerpyBotPlugin.disk.getExistingProfile(player.getDisplayName());
				for(String username : DerpyBotPlugin.disk.getAllUsernames()) if(username.toLowerCase().startsWith(word.toLowerCase())) list.add(username);
				break;
			case 1:
				//non-personal TP, must be Shared/Op to view
				//Cannot be a username
				target = DerpyBotPlugin.disk.getExistingProfile(tokens[1]);
				mustBeShared = true;
				break;
		}
		
		if(target == null) return list;
		
		for(TeleportLocation t : target.getTeleports()) if(t.name.toLowerCase().startsWith(word.toLowerCase()) && ((!mustBeShared || t.shared) || player.isOp())) list.add(t.name);
		return list;
	}
}
