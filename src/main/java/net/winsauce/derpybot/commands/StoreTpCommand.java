package net.winsauce.derpybot.commands;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.TeleportLocation;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.winsauce.derpybot.DerpyBot;

public class StoreTpCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		TeleportLocation t;
		Player p = ((Player)cs);
		DerpyPlayerProfile profile = disk.getExistingProfile(cs.getName());

		if (args.length == 1) {
			// storetp <name> OR storetp <index>
			try {
				// storetp <index>
				int index = Integer.parseInt(args[0]);
				
				if(index < 1) {
					DerpyBot.whisper("Your TP index must be a positive number", cs);
					return;
				}
				
				t = profile.getTeleport(index);

				// Alter TP's location
				if (t == null)
					DerpyBot.whisper("There is no teleport stored in slot "
							+ args[0] + " to overwrite", cs);
				else {
					t.updateLocation(p.getLocation());
					DerpyBot.whisper("Set tp \"" + t.name + "\"'s location", cs);
				}

			} catch (NumberFormatException e) {
				// storetp <name>
				try {
					Integer.parseInt(args[0]);
					DerpyBot.whisper("Cannot name a teleport a number value", cs);
				} catch (NumberFormatException f) {
					
					if(profile.getTeleport(args[0]) != null) DerpyBot.whisper("A teleport with name \"" + args[0] + "\" already exists", cs);
					else if(disk.getExistingProfile(args[0]) != null) DerpyBot.whisper("Cannot name a TP after a player: \"" + args[1] + "\"", cs);
					else {
						t = new TeleportLocation(-1, args[0], p.getLocation());
						profile.addTeleport(t);

						DerpyBot.whisper("Stored teleport \"" + args[0] + "\" at index " + (t.index), cs);
					}
				}
			}

		} else if (args.length >= 2) {
			// storetp <index> <name>
			try {
				int index = Integer.parseInt(args[0]);
				
				if(index < 1) {
					DerpyBot.whisper("Your TP index must be a positive number", cs);
					return;
				}
				
				try {
					//Catch <name> (args[1]) being a number value
					Integer.parseInt(args[1]);
					DerpyBot.whisper("Cannot name a teleport a number value", cs);
				} catch (NumberFormatException f) {

					if(profile.getTeleport(args[0]) != null) DerpyBot.whisper("A teleport with name \"" + args[0] + "\" already exists", cs);
					else if(disk.getExistingProfile(args[0]) != null) DerpyBot.whisper("Cannot name a TP after a player: \"" + args[1] + "\"", cs);
					else {
						// t = playerProfile.getTeleport(index);
						t = new TeleportLocation(index, args[1], p.getLocation());

						// Slot is not empty
						if (!profile.insertTeleport(t))
							DerpyBot.whisper("Slot " + args[0]
									+ " is already occupied by TP \"" + t.name + "\"", cs);
						// Empty slot, insert at specific point
						else DerpyBot.whisper("Stored TP \"" + args[1] + "\" in slot " + args[0], cs);
					}
				}
			} catch (NumberFormatException e) {
				DerpyBot.whisper("TP index must be a whole number value", cs);
			}
		} else {
			// storetp

			// Temp is stored in 0
			t = profile.temp;
			if (t == null) {
				// No temp location stored yet
				t = new TeleportLocation(0, "_temp", p.getLocation());
				profile.temp = t;
			} else {
				// Edit previously stored TP location
				t.updateLocation(p.getLocation());
			}

			DerpyBot.whisper("Stored location in temp", cs);
		}
	}

}
