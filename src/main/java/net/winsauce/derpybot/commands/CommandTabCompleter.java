package net.winsauce.derpybot.commands;

import java.util.ArrayList;
import java.util.List;

import net.winsauce.derpybot.plugin.DerpyBotPlugin;

import org.bukkit.entity.Player;

public class CommandTabCompleter {

	public static final String[] commandsList = {
		"cleartp",				//0
		"creative",				//1	
		"derpy",				//2
		"permission",			//3
		"private",				//4
		"property",				//5
		"returntp", "rtp",		//6 7
		"sharetp", "unsharetp", //8 9
		"storetp", "stp",		//10 11
		"tpinfo", "tpi",		//12 13
		"tp"					//14
	};
	
	
	public static List<String> tabComplete(String message, Player p) {
		String[] tokens = message.split(" +", -1);
		String commandToken = tokens[0];
		if(commandToken.charAt(0) == '!') commandToken = commandToken.substring(1);
		
		if(tokens.length == 1 && !(message.endsWith(" "))) {
			ArrayList<String> list = new ArrayList<String>();
			for(String command : commandsList) if(command.toLowerCase().startsWith(commandToken.toLowerCase())) list.add("!" + command);
			return list;
		} 
		
		int tokenIndex = tokens.length - 2; //e.g !permission giv   - give is at index 1, but -1 because excluding !Permission, so giv is arg 0. But length is 2
		String token = tokens[tokens.length-1];
		
		if(commandToken.equalsIgnoreCase(commandsList[0])) return ClearTeleportCommand.tabComplete(token, tokenIndex, DerpyBotPlugin.disk.getExistingProfile(p.getDisplayName()));
		if(commandToken.equalsIgnoreCase(commandsList[3])) return PermissionCommand.tabCompleter(token, tokenIndex);
		if(commandToken.equalsIgnoreCase(commandsList[4])) return PrivateCommand.tabComplete(token, tokenIndex);
		if(commandToken.equalsIgnoreCase(commandsList[5])) return PropertyCommand.tabComplete(token, tokenIndex);
		if(commandToken.equalsIgnoreCase(commandsList[8]) || commandToken.equalsIgnoreCase(commandsList[9]))
			return ShareTeleportCommand.tabComplete(token, tokenIndex, DerpyBotPlugin.disk.getExistingProfile(p.getDisplayName()));
		if(commandToken.equalsIgnoreCase(commandsList[12]) || commandToken.equalsIgnoreCase(commandsList[13]))
			return TeleportInfoCommand.tabComplete(token, tokenIndex);
		if(commandToken.equalsIgnoreCase(commandsList[14])) return TpCommand.tabComplete(token, tokenIndex, tokens, p);
		
		return new ArrayList<String>();
	}
}
