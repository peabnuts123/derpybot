package net.winsauce.derpybot.commands;

import net.winsauce.derpybot.DerpyPlayerProfile;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.winsauce.derpybot.DerpyBot;

public class ToggleDerpyCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		DerpyPlayerProfile playerProfile = disk.getExistingProfile(cs.getName());
		
		//[Derpy] toggled chat ON
		//[Derpy] Hello, Peabnuts123!
		
		//[Derpy] toggled chat OFF
		//[Derpy] Goodbye, Peabnuts123!
		
//		playerProfile.talkingToDerpy = !playerProfile.talkingToDerpy;
		playerProfile.toggleChat();
		
//		DerpyBot.whisper("toggled chat " + (playerProfile.talkingToDerpy ? ChatColor.GREEN + "on" : ChatColor.RED + "off"), cs);
		DerpyBot.announce((playerProfile.isTalkingToDerpy() ? "Hello" : "Goodbye") + ", " + cs.getName() 
				+ " (Chat " + (playerProfile.isTalkingToDerpy() ? ChatColor.GREEN + "ON" : ChatColor.RED + "OFF") + ChatColor.WHITE + ")", cs.getName());
	}

}
