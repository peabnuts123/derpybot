package net.winsauce.derpybot.commands;

import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.winsauce.derpybot.DerpyBot;

public class CreativeCommand extends DerpyCommand {

	@Override
	public void execute(CommandSender cs, Disk disk, String[] args) {
		Player p = (Player)cs;
		switch(p.getGameMode()) {
			case ADVENTURE:
				p.setGameMode(GameMode.SURVIVAL);
				DerpyBot.whisper("Set gamemode to " + ChatColor.YELLOW + "survival", cs);
				break;
			case CREATIVE:
				p.setGameMode(GameMode.SURVIVAL);
				DerpyBot.whisper("Set gamemode to " + ChatColor.YELLOW + "survival", cs);
				break;
			case SURVIVAL:
				p.setGameMode(GameMode.CREATIVE);
				DerpyBot.whisper("Set gamemode to " + ChatColor.GREEN + "creative", cs);
				break;
			case SPECTATOR:
				DerpyBot.whisper("I have not implemented this functionality yet...", cs);
				break;
		}
	}
	
}
