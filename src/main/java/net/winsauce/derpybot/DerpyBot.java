package net.winsauce.derpybot;

import net.winsauce.derpybot.exceptions.PlayerNotOnlineException;
import net.winsauce.derpybot.plugin.DerpyBotPlugin;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotType;

public class DerpyBot {

	public static final String USERNAME = "Derpy";
	public static final String DERPY_CHAT_PREFIX = "[" + USERNAME + "] ";
	
	public static ChatterBot bot;
	
	private static DerpyBotPlugin pluginInstance;
	
	
	public DerpyBot(DerpyBotPlugin _pluginInstance) {
		try {
			bot = new ChatterBotFactory().create(ChatterBotType.CLEVERBOT);
		} catch (Exception e) {
			DerpyBotPlugin.log("Failed to create ChatterBot Instance: " + e.getMessage());
			announce("WARNING: Failed to instantiate ChatterBotInstance!");
		}
		
		pluginInstance = _pluginInstance;
	}
	
	public static synchronized void whisper(String message, CommandSender player) {
		player.sendMessage(ChatColor.GRAY + DERPY_CHAT_PREFIX + message);
		
		DerpyBotPlugin.log(USERNAME + " -> " + player.getName() + ": " + message);
	}
	
	public static synchronized boolean announce(String message) {
		return announce(message, USERNAME);
	}
	
	public static synchronized boolean announce(String message, String user) {
		try {
			ChatColor c;
			c = DerpyBotPlugin.chatColourHandler.getPlayerChatColour(user);
			pluginInstance.getServer().broadcastMessage(c + DERPY_CHAT_PREFIX + ChatColor.WHITE + message);
			return true;
		} catch (PlayerNotOnlineException e) {
			return false;
		}
	}
}
