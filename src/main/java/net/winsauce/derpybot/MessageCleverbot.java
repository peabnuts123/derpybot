package net.winsauce.derpybot;

import net.winsauce.derpybot.plugin.DerpyBotPlugin;

import com.google.code.chatterbotapi.ChatterBotSession;

public class MessageCleverbot extends Thread {

	private String message;
	private DerpyPlayerProfile playerProfile;
	private ChatterBotSession botSession;
	
	public MessageCleverbot(String message, DerpyPlayerProfile profile, ChatterBotSession botSession) {
		this.message = message;
		this.playerProfile = profile;
		this.botSession = botSession;
		
		start();
	}
	
	public void run() {
		try {
			String response = botSession.think(message.replaceAll("(?i)derpybot|(?i)derpy bot|(?i)derpy", "CleverBot")).replaceAll("(?i)cleverbot", "DerpyBot");
			Thread.sleep((int)((Math.random() + 1)*1000));
			
			DerpyBot.announce(response, playerProfile.getName());
		} catch (Exception e) {
			DerpyBotPlugin.log("Exception in MessageCleverbot.run: " + e.getMessage());
			DerpyBot.announce("I failed to think, derp!");
		}
	}

	
}
