package net.winsauce.derpybot.exceptions;

public class PlayerNotOnlineException extends Exception {

	private static final long serialVersionUID = -5381983530749234091L;
	
	public PlayerNotOnlineException() {
		super("The specified player is not currently online!");
	}
}
