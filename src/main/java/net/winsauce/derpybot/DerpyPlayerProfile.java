package net.winsauce.derpybot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.winsauce.derpybot.exceptions.PlayerNotOnlineException;
import net.winsauce.derpybot.plugin.ChatColourHandler;
import net.winsauce.derpybot.plugin.DerpyBotPlugin;
import net.winsauce.derpybot.plugin.Disk;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.google.code.chatterbotapi.ChatterBotSession;

public class DerpyPlayerProfile {

	
	
	//Must be lower case
	public enum Permission {
		su;
		
		public static boolean isPermission(String p) {
			try { 
				return valueOf(p.toLowerCase()) != null;
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
	}
	
	public enum Property {
		saveinventory;
		
		public static boolean isProperty(String p) {
			try { 
				return valueOf(p.toLowerCase()) != null;
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
	}
	
	private String playerName;
	
	private List<TeleportLocation> teleports;
	public TeleportLocation temp;
	public Location previous;
	
	public boolean inPrivate;//, talkingToDerpy;
	
	private ChatterBotSession chatBotSession;
	
	private List<Property> properties;
	
	private List<Permission> permissions;
	
	public static Disk disk;
	
	public DerpyPlayerProfile(String _playerName, List<TeleportLocation> _teleports, List<Property> _properties, List<Permission> _permissions) {
		playerName = _playerName;
		teleports = _teleports;
		
		properties = _properties == null ? new ArrayList<Property>() : _properties;
		permissions = _permissions == null ? new ArrayList<Permission>() : _permissions;
		
		chatBotSession = null; 
	}
	
	public void logOut() {
		temp = null;
		previous = null;
		inPrivate = false;
		chatBotSession = null;
	}
	
	
	public boolean isTalkingToDerpy() {
		return chatBotSession != null;
	}
	
	public void toggleChat() {
		if(isTalkingToDerpy()) {
			//Player is currently talking to Derpy, turn off
			chatBotSession = null;
		} else {
			//Player is not talking to Derpy, turn on
			chatBotSession = DerpyBot.bot.createSession();
		}
	}
	
	public void chatToDerpy(String message) {
		new MessageCleverbot(message, this, chatBotSession);
	}
	//PERMISSIONS
	
	public boolean revokePermission(Permission permissionKey) {
		if(hasPermission(permissionKey)) {
			permissions.remove(permissionKey);
			onChanged();
			return true;
		} else return false;
	}
	
	public boolean givePermission(Permission permissionKey) {
		if(!hasPermission(permissionKey)) {
			permissions.add(permissionKey);
			onChanged();
			return true;
		} else return false;
	}
	
	public boolean hasPermission(Permission permissionKey) {
		return permissions.contains(permissionKey);
	}
	
	
	//PROPERTIES
	
	public boolean revokeProperty(Property propertyKey) {
		if(hasProperty(propertyKey)) {
			properties.remove(propertyKey);
			onChanged();
			return true;
		} else return false;
	}
	
	public boolean giveProperty(Property propertyKey) {
		if(!hasProperty(propertyKey)) {
			properties.add(propertyKey);
			onChanged();
			return true;
		} else return false;
	}
	
	public boolean hasProperty(Property permissionKey) {
		return properties.contains(permissionKey);
	}
	
	public ChatColor getChatColour() {
		try {
			return DerpyBotPlugin.chatColourHandler.getPlayerChatColour(getName());
		} catch (PlayerNotOnlineException e) {
		}
		
		DerpyBotPlugin.log("GET CHAT COLOUR HAS FAILED THIS IS IMPOSSIBLE WHAT");
		return ChatColor.BLACK;
	}
	
	public ChatColor getRequestedChatColour() {
		int colour = ChatColourHandler.validColours.length - Math.abs((playerName.hashCode() * playerName.length()) % ChatColourHandler.validColours.length) - 1;
		
		return ChatColourHandler.validColours[colour];
	}
	
	public void onChanged() {
		disk.writeProfile(this);
	}
	
	public String getName() {
		return playerName;
	}
	
	public void addTeleport(TeleportLocation tp) {
		for (int i = 0; i < teleports.size(); i++) {
			if(teleports.get(i).index != (i+1)) {
				teleports.add(i, tp);
				tp.index = (i+1);
				onChanged();
				return;
			}
		}
		
		teleports.add(tp);
		tp.index = teleports.size();
		onChanged();
	}
	
	public boolean insertTeleport(TeleportLocation tp) {
		for (int i = 0; i < teleports.size(); i++) {
			
			if(teleports.get(i).index == tp.index) return false;
			
			if(teleports.get(i).index > tp.index) {
				teleports.add(tp);
				onChanged();
				return true;
			}
		}
		
		teleports.add(tp);
		onChanged();
		return true;
	}
	
	public TeleportLocation getTeleport(String name) {
		for(TeleportLocation l : teleports) if(l.name.equalsIgnoreCase(name)) return l;
		return null;
	}
	
	public TeleportLocation getTeleport(int index) {
		for(TeleportLocation l : teleports) if(l.index == index) return l;
		return null;
	}
	
	public ArrayList<TeleportLocation> getTeleports() {
		return new ArrayList<TeleportLocation>(teleports);
	}
	
	public boolean removeTeleport(int index) {
		TeleportLocation t = getTeleport(index);
		if(t != null) return teleports.remove(t);
		else return false;
	}
	
	public boolean removeTeleport(String name) {
		TeleportLocation t = getTeleport(name);
		if(t != null) return teleports.remove(t);
		else return false;
	}
	
	public ArrayList<Property> getProperties() {
		return new ArrayList<Property>(properties);
	}
	
	public ArrayList<Permission> getPermissions() {
		return new ArrayList<Permission>(permissions);
	}
	
	public void setTeleports(List<TeleportLocation> _teleports) {
		teleports = _teleports;
		onChanged();
	}
	
	public void setProperties(List<Property> _properties) {
		properties = _properties;
		onChanged();
	}
	
	public void setPermissions(List<Permission> _permissions) {
		permissions = _permissions;
		onChanged();
	}
	
	public static DerpyPlayerProfile newProfile(String playerName) {
		return new NewLoginProfile(playerName, new ArrayList<TeleportLocation>(), new ArrayList<Property>(), new ArrayList<Permission>());
	}
	
	public static DerpyPlayerProfile readProfile(HashMap<String, Object> profileEntry) {
		/* 
		 * Entries:
		 * ========
		 * name			- Player Name
		 * teleports	- List of TeleportLocation String definitions
		 * properties	- List of property keys; e.g "preserveInventory"
		 * permissions	- List of permission keys; e.g "su"
		 * 
		 */
		
		List<Property> properties = new ArrayList<Property>();
		List<String> propertyStrings = (List<String>)profileEntry.get("properties");
		if(propertyStrings != null) for(String s : propertyStrings) properties.add(Property.valueOf(s));

		List<Permission> permissions = new ArrayList<Permission>();
		List<String> permissionStrings = (List<String>)profileEntry.get("permissions");
		if(permissionStrings != null) for(String s : permissionStrings) permissions.add(Permission.valueOf(s));
		
		List<TeleportLocation> tpLocations = new ArrayList<TeleportLocation>();
		List<String> tpEntries = (List<String>) profileEntry.get("teleports");
		if(tpEntries != null) for(String tpEntry : tpEntries) tpLocations.add(TeleportLocation.readTeleportLocation(tpEntry));
		
		return new DerpyPlayerProfile(profileEntry.get("name").toString(), tpLocations, properties, permissions);
	}

	@Override
	public String toString() {
		return "Profile: " + playerName;
	}
	
	public static void setDiskInstance(Disk diskInstance) {
		disk = diskInstance;
	}
	
	/**
	 * To establish whether a profile is a new-login or not.
	 * @author Peabnuts123
	 */
	public static class NewLoginProfile extends DerpyPlayerProfile {
		
		public boolean firstLogin;
		
		public NewLoginProfile(String _playerName, List<TeleportLocation> _teleports, List<Property> _properties, List<Permission> _permissions) {
			super(_playerName, _teleports, _properties, _permissions);
			
			firstLogin = true;
			
			this.onChanged();
		}
	}
}
