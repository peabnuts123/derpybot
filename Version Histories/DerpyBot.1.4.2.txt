DerpyBot 1.4.2 ChangeList
=========================

First Official Release!

Change List:

- Teleports can now be referenced by index. eg. !tp 4 to teleport to the TP stored in slot 4

- Teleports now fail when trying to teleport across worlds (i.e Overworld -> Nether)
	If a TP is stored whilst in the Nether, for example, you can TP to it, IF you are in the nether

- Chat-Bot communication with Derpy
	See /help derpy

- Full tab-autocompletion on all commands and parameters 
	e.g: (assume player has teleports "house" and "lake")
		type "!tp ho"
		press tab
		"house" is inserted resulting in "!tp house" in your chat bar
	
	Player names are not autocompleted at this point unless they are online
		
- Commands Added:
	derpy
	permission 
	storeTp 
	tp
	tpInfo
	shareTp
	unShareTp
	returntp
	cleartp
	private
	creative
	
- Properties Added
	Currently only one property "saveInventory"
	
- Aliased commands with !
	Allows players to use '!' instead of '/' to use commands 

- Added Presence of Derpy
	Derpy can announce to server/whisper specific players
	
- Chat Colour codes
	Re-Done since old implementation
		- Collision Algorithm implemented for players with same colour code
		- Shuffled everyone's default colours
		
- Permission Handler
	Permission groups now present
		- currently only one group, allows !creative command access
		- means OP is strictly reserved for Admins now